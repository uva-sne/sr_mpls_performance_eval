#####
Setup
#####

The base setup is as follows. One server serves as a traffic generator and receiver, the other serves as the platform for running the VNFs.
This way the CPU usage of the traffic generation does not affect the VNF performance.

.. image:: images/highlevel-mc7-mc8-1.png


Hardware
========
The MC7 and MC8 servers from the MNS lab are used.  
MC7 has the following hardware:  
- E5-1620 v4 (8 core) @ 3.50GHz
- 64GB RAM
- 2TB Disk
- Netronome 4000 Smart NIC


MC8 has the following hardware:  
- E5-1620 v4 (8 core) @ 3.50GHz
- 64GB RAM
- 2TB Disk
- Mellanox ConnectX-4 Lx

The NICs are interconnected with 25Gbit DACs.

Software
========
As per the shown high level image, MC7 serves as the packet generator with MoonGen.
MC8 serves to run the VNFs.
Both servers are running Ubuntu 18.04.5 LTS on the BPF Next kernel 5.2.0-rc5+.  

VNFs on MC8
***********
To allow the VNFs to be loaded on MC8, the BPF Next kernel 5.2.0-rc5+ has been loaded.
By default, the Mellanox NIC discards packets that are not explicitely destined for the server.
These will be quietly discarded. If running tcpdump against the NIC, these packets _will_ be accepted by the NIC.
The solution is to set the NIC in promiscious mode, allowing it to accept all packets, malformed and not destined for the server.
This can be done with the following command: `ip link set <if> promisc on`


PCIe Passthrough
================
The packet generation with DPDK on the MC7 with the Netronome 4000 NIC proved not to be stable.
The DPDK NIC driver would sometimes crash taking down the entire system.
To alleviate this issue, MoonGen has been setup in a virtual machine with the Netronome NIC passed through to the VM.
To set this up, the guides on: [pciepassthrough1]_, [pciepassthrough2]_ and [pciepassthrough3]_ have mainly been followed.  
This results in the setup:  

.. image:: images/highlevel-mc7-mc8-passthru1.png

IOMMU & Huge pages
************
The first step is to enable IOMMU and setting a high number of huge pages
so that the vm can be backed by huge pages.  
| 
Edit /etc/default/grub, adding ``intel_iommu=on``  
::
    ...
    GRUB_CMDLINE_LINUX_DEFAULT="quiet splash intel_iommu=on"  
    ...

  
Edit /etc/sysctl.conf  
::
    vm.nr_hugepages=10240
    vm.hugetlb_shm_group=36


Binding VFIO
************
By default, the Netronome card is bound to the NFP driver, when using DPDK it is bound to the igb_uio driver.
For PCIe passthrough it needs to be bound to the VFIO driver.  
  
Edit /etc/modules
::
    vfio
    vfio_iommu_type1
    vfio_pci
    vfio_virqfd


Edit /etc/mkinitcpio.conf
::
    HOOKS=(modconf)
    MODULES=(vfio_pci vfio vfio_iommu_type1 vfio_virqfd)


Edit /etc/initramfs-tools/modules
::
    vfio
    vfio_iommu_type1
    vfio_virqfd
    vfio_pci


| Binding the NIC to VFIO:  
Edit /etc/modprobe.d/vfio.conf
::
    options vfio-pci ids=19ee:4000

| Stop the NFP driver from binding the NIC:
Edit /etc/modprobe.d/nfp.conf
::
    softdep nfp pre: vfio-pci

The initramfs has to be regenerated: ``update-initramfs -u -k all``
And finally the machine has to be rebooted. Then `lspci -v` should show that `vfio-pci` is used for the Netronome card.
::
    03:00.0 Ethernet controller: Netronome Systems, Inc. Device 4000
        Subsystem: Netronome Systems, Inc. Device 4001
        Kernel driver in use: vfio-pci
        Kernel modules: nfp

Starting the VM
***************
Finally, the VM can be started using huge pages and with the NIC using the Vagrantfile in the folder ``moongen-vagrant``.
Once started, the VM should show the NIC in `lspci`.

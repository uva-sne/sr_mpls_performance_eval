################
Hardware offload
################

BPF programs can be hardware offloaded to capable NICs for far increased performance. 
However, there are some caveats to this and thus with the current implementation of the VNFs it is not possible to offload them.
This section discusses the various things explored and discovered whilest trying to offload the programs.
Lukasz had already made some notes on his experimentation as well: https://bitbucket.org/uva-sne/ron19_sr/src/master/docs/source/nfp_memo.md

Limits & Caveats
================
As per [offload2]_ the only allowed functions are:
::
    bpf_map_lookup_elem() 
    bpf_map_update_elem() 
    bpf_map_delete_elem() 
    bpf_get_prandom_u32() 
    bpf_perf_event_output() 
    bpf_xdp_adjust_head() 
    bpf_xdp_adjust_tail()

Additionally, only a limited set of return codes are allowed for TC classifiers:
https://github.com/torvalds/linux/blob/a2d79c7174aeb43b13020dd53d85a7aefdd9f3e5/drivers/net/ethernet/netronome/nfp/bpf/verifier.c#L311
Importantly, TC redirect is not allowed, which would be important to test throughput or to just process packets in the BPF VNF and send it back out, like the SR-Firewall.

To pas the packets from the hardware to the OS, TC_ACT_TRAP can be used:
https://elixir.bootlin.com/linux/latest/source/include/uapi/linux/pkt_cls.h#L62

With these limitations, it would not be possible to do tail calls (bpf_tail_call), or to push and pop MPLS headers (which relies on bpf_skb_adjust_room, with Lukasz kernel patches).

BPF on DPDK
***********
BPF programs can also be ran on DPDK drivers for increased speed. [dpdkbpf1]_ [dpdkbpf2]_
However, there are many limitations. For example, in the current version eBPF maps and tail calls are not supported.
Both of these are key to the currently implemented VNFs and thus the VNFs cannot be executed on top of DPDK.

XDP
===
The current set of VNFs is implemented as TC classifiers. As discussed, these have many limitations making hardware offload infeasible.
TC classifiers are executed fairly late in the networking stack
and thus are more limited in the hardware offloading and performance they can offer.
XDP 
I see more possibilities in implementing the MPLS support in XDP.
In the verifier, there seem to be far fewer restrictions on the function in XDP. All return codes seem to be allowed and changing the size
of the packet for pushing and popping MPLS labels seems to be supported (bpf_xdp_adjust_head).

Only the main MPLS header parsing will need to be changed, most of the actual VNF functionality does not change between TC and XDP, only the initial
packet parsing.

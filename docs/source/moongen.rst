###############################
Traffic generation with MoonGen
###############################

This section contains some instructions on how to run MoonGen as well as some of the results.

Git Repositories and Patches
============================
The base version of MoonGen does not work with the Netronome 4000 NIC
in mc7. This has been fixed in pull request, but as of November 2020 this pull request has not been
merged into the main MoonGen repo.
See:
https://github.com/emmericp/MoonGen/issues/195
https://github.com/emmericp/MoonGen/pull/250

Additionally, by default MoonGen does not allow for native MPLS packet generation.
I've submitted a pull request for this to the main repo, but again this has not been merged yet:
https://github.com/libmoon/libmoon/pull/101

I have forked MoonGen and the underlying Libmoon to the SNE Bitbucket:
https://bitbucket.org/uva-sne/libmoon/src/master/
https://bitbucket.org/uva-sne/moongen/src/master/

The branch 'mpls_w_update' contains the patch for the Netronome 4000 NIC as well as the MPLS header support.
The MPLS header support is also present in the seperate branch 'mplsheader' and thus can be used to apply patches
to any newer version of MoonGen (or patched for Mellanox cards if needed).

https://bitbucket.org/uva-sne/libmoon/branches/


Netronome Packetgen MPLS extension
==================================
Netronome has developed a packet generator as part of MoonGen (/examples/netronome-packetgen).
This packet generator does timestamping via the payloads of the packets.
For the performance testing, I've taken this packetgenerator and changed it to use SR-MPLS packets instead:
https://bitbucket.org/uva-sne/moongen/src/mpls_w_updatelibmoon/examples/netronome-packetgen/packetgen-mpls.lua

Importantly, the number of MPLS labels and such cannot be changed from the commandline as I did not write cli argument parsing
for labels. In order to change the labels, the contents of the lua file have to be changed.
##################
Traffic Generation
##################

There are various tools capable of traffic generation.
There are four key points to decide which one to use for the performance evaluation
1. Speed. Can the traffic generator generate packets fast enough to saturate the VNFs to evaluate their performance
2. Ease of use. How hard is it to setup the software in the system?
3. Flexibility. Is it possible to generate multiple types of packets?
4. State or stateless. Can the packet generator receive the packets and if do some sort of state calculation


For the traffic generation to evaluate the performance I had a look at various packet generators to potentially use.
For the ease of use and flexibility, the main point to consider is that the generator has to be able to generate SR-MPLS packets.
For most VNFs, the IP header and inner TCP/UDP header plus payload do not matter, but for the SR-Firewall this is relevant.


Python + Scapy
==============
Used in: https://bitbucket.org/uva-sne/nfv/src/master/sr_proxy/tests/packet.py
Manual packet generation with Python and scapy is possible.
This is not meant for speed and takes quite a bit of programming to set up.
However it is not dependent on any hardware and can be setup for testing.
This is the case for the packet.py used to test the sr proxy.


Etherate
========
https://github.com/jwbensley/Etherate

Etherate is a basic tool with no special hardware or software requirements.
It does not need specific NICs, but also does not offer particularly high performance as it is fully CPU bound.
The software has various basic options for packet generation and allows the loading of manually specified hex files, which allows any type of packet to be generated and sent out.
It natively supports MPLS labels and can stack any labels with any flags, making it suible for testing the VNFs.
It can be ran in receive mode to see what the throughput achieved through the system is.
This tool is mostly suited for basic testing with a very quick setup time. 


Pktgen
======
https://github.com/Pktgen/Pktgen-DPDK

Low level packetgeneration on DPDK. Requires a NIC that supports DPDK, but is suited to generate packets at line rate.
Seems to have fairly low ease of use and thus the generation of SR-MPLS packets is probably
fairly cumbersome to do.

Not used for the performance evaluation due to the low ease of use.

MoonGen
=======
https://github.com/emmericp/MoonGen

As Pktgen for DPDK is a rather low level tool, MoonGen has been created to offer better ease of use.
It can generate packets at line rate with DPDK supported NICs and receive them as well.

It does not natively support MPLS and no SR-MPLS, but allows for fairly easy adding of packet definitions.
The actual generation of packets can perform any kind of calculation or state comparision for receiving and sending of packets.


Cisco T-Rex
===========
https://trex-tgn.cisco.com/

Another DPDK based packet generator. Seems to be mainly aimed at high level ease of use and thus is less flexible.
Documentatation does not make it clear how easy it is to generate SR-MPLS packets or perform further calculations on the sent and received packets.
Not used because MoonGen seemed to work very well.


Conclusion
==========
Etherate has been used as the basic tool for verifying the functionality of the setup as it can easily be setup and natively support SR-MPLS packet generation.
MoonGen has been used for the performance evaluation with a custom programmed generation script.
.. target-notes::
.. MoonGen & Setup 
.. [pciepassthrough1] Passing a Network card to a KVM vm guest because we are too lazy to configure SR-IOV, http://unixwars.blogspot.com/2019/05/passing-network-card-to-kvm-vm-guest.html
.. [pciepassthrough2] PCI passthrough adventures with libvirt., https://aixxe.net/2018/12/qemu-ovmf-adventures
.. [pciepassthrough3] PCI(e) Passthrough, https://pve.proxmox.com/wiki/PCI(e)_Passthrough

.. Hardware offload
.. [offload2] Ever Deeper with BPF – An Update on Hardware Offload Support, https://www.netronome.com/blog/ever-deeper-bpf-update-hardware-offload-support/
.. [dpdkbpf1]  51. Berkeley Packet Filter Library, http://doc.dpdk.org/guides/prog_guide/bpf_lib.html
.. [dpdkbpf2] DPDK+eBPF, https://www.dpdk.org/wp-content/uploads/sites/35/2018/10/pm-07-DPDK-BPFu6.pdf
########
Examples
########

This section contains various example commands for traffic generation and testing with results.
In addition it contains a bunch of other commands useful for debugging, deploying and other various tasks.


Etherate
========
Basic etherate TX and RX to test if packets are correctly being passed.
Expects one label to be popped, sends out one packet per second.
::
    ./etherate  -i enp3s0np0np0 -L 1012 0 255 -L 1004 0 255  -G -F 1
    ./etherate  -i enp3s0np1np1 -L 1004 0 255 -r

Etherate cannot do full 10Gbit and crashen on 10GBit cards:
https://github.com/jwbensley/Etherate/issues/40

Stat watch
==========
Stat watch can be used to watch the various counters on the NICs to observe the dropped and passed packets.
Many of the counters are not needed and stat watch can filter these out.
Some useful commands with filters:

::

    root@mc8:~# python stat_watch.py -c -f 't|rx_.+' -x '.*x\d.*' -x '.*vport.*' -x 'ch.*' enp3s0f0
    root@mc8:~# python stat_watch.py -c -f 't|rx_.+' -x '.*x\d.*' -x '.*vport.*' enp3s0f1


DPDK Driver binding
===================
The `dpdk_nic_bind.py` script can be used to bind specific drivers to NICs.
This is useful to bind dpdk to a specific nic, or unbind it when needed.
Some example commands:
::
    python ~/dpdk_nic_bind.py --status # displays the driver status for all nics
    python ~/dpdk_nic_bind.py -u 0000:03:00.0 # unbinds all drivers from the netronome nic
    python ~/dpdk_nic_bind.py -b igb_uio 0000:03:00.0 # binds the dpdk driver to the nic
    python ~/dpdk_nic_bind.py -b nfp 0000:03:00.0 # binds the 'normal' netronome driver to the nic
    python ~/dpdk_nic_bind.py -u 0000:02:00.1 #Unbinds the 'other' NIC in mc7


MoonGen Operation
=======================
After cloning it has to be built, then the driver bound to the needed NIC.

Depending on the version, the linking does not always go correctly and can be fixed by doing:

::

    export LD_LIBRARY_PATH=$HOME/MoonGen/build/libmoon/tbb_cmake_build/tbb_cmake_build_subdir_release:$LD_LIBRARY_PATH

Basic MPLS flows can started as follows:

::

    ./moongen-simple start mpls-stack-flood:0:1:rate=1p/s

The contents of the above flow can be changed by editing the file https://bitbucket.org/uva-sne/moongen/src/mpls_w_updatelibmoon/flows/mpls-flood.lua

The changed Netronome packet generator can be ran with:

::

    ./build/MoonGen ./examples/netronome-packetgen/packetgen-mpls.lua -tx 0 -rx 1
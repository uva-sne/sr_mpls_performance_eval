############
Introduction
############

The NFVs developed as part of RoN19 and RoN20 have been tested for service chaining in conjunction with NorthStar.
However its performance was not yet known. This documentation contains the results from the performance evaluation
on the MNS group hardware. To evaluate the performance of the created VNFs, traffic has to be generated, processed to the VNFs
and received. A baremetal setup has been used with 25Gbit NICs with MoonGen for traffic generation.

This documentation describes architecture, the software and the results obtained.
In addition it describes the limitations with regard to hardware offloading.

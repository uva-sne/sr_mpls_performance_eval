#!/bin/bash
#
# This script makes and loads VNFs for MC8
# Compiles and loads for the BPF tail call setup

# Ensure everything is echo-ed to stdout
set -x
set -e
source common.sh || source scripts/common.sh

echo "Creating interfaces"
make dummy_if


echo "Make nfv switch for BPF"
make clean
make CLANG_FLAGS='-D SWITCH_BPF_IDXS=1' nfv_switch.bpf

echo "Making SR-MPLS Dummy"
(cd mpls_dummy && make CLANG_FLAGS='-D SWITCH_BPF_IDXS=1' mpls_dummy.bpf)


echo "Clearing NFV Switch and reloading"
tc filter del dev enp3s0f0  ingress || tc qdisc add dev enp3s0f0  clsact
tc filter add dev enp3s0f0  ingress bpf da obj nfv_switch.bpf sec switch

echo "Setting debug level"
make debug_high


echo "Attaching dummy"
tc filter del dev dummy0 ingress || tc qdisc add dev dummy0 clsact
(cd mpls_dummy && tc filter add dev dummy0 ingress bpf da obj mpls_dummy.bpf sec mpls_dummy)
make -C mpls_dummy debug_high

echo "Ensuring out will redirect to enp3s0f1"
IF_IDX=$(ip link| grep -Eo '.* enp3s0f1' | grep -Eo '[0-9]+: ' | sort | tr -d :)
bpftool map update pinned /sys/fs/bpf/tc/globals/MPLS_OUT_IF_MAP key hex 0x0 0x0 0x0 0x0 value $IF_IDX 0 0 0

echo "Updating FUNC SWITCH MAP"
python3 scripts/update_func_switch_map.py

echo "Creating base map"
# Not needed, loading of VNFs adds this already, it is here as reference
# bpftool map create /sys/fs/bpf/tc/globals/SID_IF_ID_ARRAY type array  key 4 value 4 entries 8 name SID_ARRAY_MAP

echo "Creating SID 1012 map"
bpftool map create /sys/fs/bpf/tc/globals/SID_1012_BPF_ID_ARRAY type array  key 4 value 4 entries 8 name SID_1012_BPF_MAP || echo "Already exists"
MAP_ID=$(bpftool map show -f | grep -w SID_1012_BPF_MAP | grep -Eo '[0-9]+: ' | sort | tr -d :)

echo "Adding BPF ID as key 2 in the map"

get_prog_id "mpls_dummy"
upd_progarray "SID_1012_BPF_ID_ARRAY" 2 "mpls_dummy"


echo "Inserting $MAP_ID in the SID_TO_BPF_IDXS map for sid 1012"
bpftool map update pinned /sys/fs/bpf/tc/globals/SID_TO_BPF_IDXS key hex 0xf4 0x03 0x00 0x00 value id $MAP_ID

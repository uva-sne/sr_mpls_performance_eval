#!/bin/bash
#
# This script contains a basic traffic generation command
#
set -x
set -e

# Bind the nic to the dpdk driver
python ~/dpdk_nic_bind.py -b igb_uio 0000:03:00.0

# Set the ld linking needed for libmoon
# (Might not be needed for later version of MoonGen/libmoon)
export LD_LIBRARY_PATH=$HOME/MoonGen/build/libmoon/tbb_cmake_build/tbb_cmake_build_subdir_release:$LD_LIBRARY_PATH

# Start the traffic generation for 10 seconds
./build/MoonGen ./examples/netronome-packetgen/packetgen-mpls.lua -tx 0 -rx 1 -to 10

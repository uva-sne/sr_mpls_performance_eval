# SR MPLS NFV Performance Testing

This repository contains documentation and scripts for the performance testing of the SR MPLS NFVs
using various tools on the MC7 and MC8 servers

![High level network](https://bitbucket.org/uva-sne/sr_mpls_performance_eval/raw/5ee4ac9ac353acc1d73a93baf1ca3de6a19b8711/docs/source/images/highlevel-mc7-mc8-1.png)

## Making the documentation
The documentation can be made into folder for html docs
```
make html
```

## Using the scripts
The scripts do not know the directory structure and thus will fail if executed from the wrong directory.
The traffic generation scripts should be executed from the `MoonGen` directory used.
The mc8 scripts should be executed from the `nfv` directory containing all the code.
Additionally, some tools have been taken:
https://github.com/vpp-dev/dpdk/blob/master/tools/dpdk_nic_bind.py
https://github.com/Netronome/nfp-drv-kmods/blob/master/tools/stat_watch.py